# Hunger Games

You live in a tribe of people. Your tribe needs food and sends its members out
to hunt for food in teams of two. The game is divided into rounds and consists
of the following:

## I. Food and Winning

Each player begins the game with 300(P−1) units of food, where P is the number
of players.

If after any round you have zero food, you will die and no longer be allowed to
compete.

The game can end in two ways. After a large number of rounds, there will be a
small chance each additional round that the game ends. Alternatively, if there
is only one person left with food then the game ends. In each case, the winner
is the person who has the most food when the game ends.

The game is designed to encourage both cooperation and competition between
players at different points in the game. Determining the right balance leads to
a winning strategy.

## II. Hunts

Each round is divided into hunts. A hunt is a game played between you and one
other player. Each round you will have the opportunity to hunt with every other
remaining player, so you will have P−1 hunts per round, where P is the number of
remaining players.

In a hunt you can make the decision to hunt (become a hunter) or feign sickness
and not hunt (become a slacker). Hunting is an active activity, so you will eat
6 units of food if you decide to hunt, and 2 units of food if you decide not to
hunt. If both you and your partner do not hunt, then you get 0 food from the
hunt. If only one of you decides to hunt then the hunt returns 6 units of food.
If both of you decided to hunt then the hunt returns 12 units of food. The total
food return of the hunt is split evenly between you and your partner. So, for
example, if both of you decide to hunt, your individual net food gain/loss from
the hunt would be 12/2−6=0 units, whereas if both of you decided not to hunt
your individual net food gain/loss is 0/2−2=−2 food.

## III. Reputation

Each player has a reputation R, which is defined as R=H/(H+S), where H is the
number of times you chose to hunt and S is the number of times you chose to
slack (not hunt), both counted from the beginning of the game. Each player
begins with a reputation of 0.

Each round, your tribe can save some time and hunt extra food if enough people
opt to hunt. Each round, a random integer m, with 0<m<P(P−1) will be chosen. If
the sum of the number of times players choose to hunt is greater than or equal
to m for the upcoming round, then everyone in the tribe gets 2(P−1) extra units
of food after the round, which means that on average everyone gets 2 units of
food per hunt. Before each round, you will find out the value of m.

Before each round you will be given: which number round you are in (from 1 to
∞), your food, your reputation, m, and an array with the reputation of each
remaining member of the tribe when the round started. The location of each
player in the array will be randomized after each round. You should decide your
moves for that round for each player you will play against, and return an array
of your decisions. After each hunt you will be given the food earned from each
hunt you played.
