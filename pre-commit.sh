#!/bin/bash

EXIT=0

pushd . > /dev/null

git_root=$(git rev-parse --show-toplevel)
git_stage=$(git diff-index --cached HEAD | cut -f2)

cd $git_root

# make indentation consistent
for file in $git_stage; do
    grep -q $'\t' $file && sed -i 's/\t/    /g' $file
done

# fail on pep8 violations
git_stage_py=$(echo $git_stage | tr ' ' '\n' | grep 'py$')
if [[ -n $git_stage_py ]]; then
    pep8 $git_stage_py || EXIT=1
fi

popd > /dev/null
exit $EXIT
