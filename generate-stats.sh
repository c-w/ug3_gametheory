#!/bin/bash

base_dir="$(dirname $(readlink -f $0))"
src_dir="$base_dir/src"
out_dir="$base_dir/data"

python $src_dir/stats.py $@ --save-to=$out_dir
