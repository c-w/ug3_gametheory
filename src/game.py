import ast
import random
import os
import sys
import threading

import util
import plot
from player import Player
from player import HUNT
from player import SLACK


MAX_ROUNDS = 10000
END_CHANCE = 0.05


class ScorableWrapper(object):
    def __init__(self, wrapped_player, num_players):
        self.wrapped = wrapped_player
        self.food = 300 * (num_players - 1)
        self.reputation = 0
        self.food_history = []
        self.reputation_history = []
        self._times_hunted = 0
        self._times_slacked = 0
        self.rounds_lived = 0

    def hunt_choices(self, round_num, food, rep, m, reps):
        choices = self.wrapped.hunt_choices(round_num, food, rep, m, reps)
        self._times_hunted += choices.count(HUNT)
        self._times_slacked += choices.count(SLACK)
        self.reputation = float(self._times_hunted) / (self._times_slacked +
                                                       self._times_hunted)

        return choices

    def hunt_outcomes(self, food_earnings):
        self.wrapped.hunt_outcomes(food_earnings)
        self.food += sum(food_earnings)

    def round_end(self, award, m, number_hunters):
        self.wrapped.round_end(award, m, number_hunters)
        self.food += award
        self._remember_history()
        if not self.is_dead():
            self.rounds_lived += 1

    def is_dead(self):
        return self.food <= 0

    def _remember_history(self):
        self.food_history.append(self.food)
        self.reputation_history.append(self.reputation)

    def __repr__(self):
        return '%s(food=%d, reputation=%.2f)' % (self.wrapped, self.food,
                                                 self.reputation)

    def __gt__(self, other):
        if self.food > other.food:
            return True
        if self.food < other.food:
            return False
        return self.reputation > other.reputation

    def __eq__(self, other):
        return self is other


class Game(object):
    def __init__(self, players):
        self.players = [ScorableWrapper(p, len(players)) for p in players]
        self.round_num = 0
        self._player_choices = []
        self._dead_players = []

    def player_ranking(self):
        all_players = self._dead_players + self.players
        ranking = sorted(all_players, key=lambda p: p.reputation, reverse=True)
        ranking = sorted(ranking, key=lambda p: p.food, reverse=True)
        ranking = sorted(ranking, key=lambda p: p.rounds_lived, reverse=True)

        cur_rank = 1
        ranked_players = []
        for i, player in enumerate(ranking):
            ranked_players.append((cur_rank, player))
            try:
                next_player = ranking[i + 1]
            except IndexError:
                break
            rounds_same = player.rounds_lived == next_player.rounds_lived
            food_same = player.food == next_player.food
            reputation_same = player.reputation == next_player.reputation
            if not (rounds_same and food_same and reputation_same):
                cur_rank += 1

        return ranked_players

    def run(self, logfile=None):
        rounds = []
        rounds.append(repr(self))
        while not self.is_over():
            self.play_round()
            rounds.append(repr(self))

        if logfile is not None:
            logfile.writelines('\n'.join(rounds))

    def is_over(self):
        if len(self.players) <= 1:
            return True
        return self.round_num > MAX_ROUNDS and random.random() > END_CHANCE

    def play_round(self):
        # setup: randomization
        self.round_num += 1
        self._player_choices = []
        random.shuffle(self.players)
        P = len(self.players)
        m = util.random_int(0, P * (P - 1))

        # phase 1: players make decisions
        hunts = {}
        for player in self.players:
            other_players = [p for p in self.players if p != player]
            reputations = [p.reputation for p in other_players]
            choices = player.hunt_choices(self.round_num, player.food,
                                          player.reputation, m, reputations)
            hunts[player] = dict(zip(other_players, choices))
            self._player_choices.extend(choices)

        # phase 2: process hunt outcomes
        for player in self.players:
            food_earnings = []
            for other_player, decision in hunts[player].iteritems():
                other_decision = hunts[other_player][player]
                food_earnings.append(score(decision, other_decision))
            player.hunt_outcomes(food_earnings)

        # phase 3: reward altruism
        number_hunters = self._player_choices.count(HUNT)
        award = 2 * (P - 1) if number_hunters > m else 0
        for player in self.players:
            player.round_end(award, m, number_hunters)

        # cleanup: eliminate starved players
        live_players = []
        for player in self.players:
            if player.is_dead():
                self._dead_players.append(player)
            else:
                live_players.append(player)
        self.players = live_players

    def __repr__(self):
        self.players.sort(reverse=True)
        return 'Round %d: %s' % (self.round_num, self.players)

    @staticmethod
    def create_from_file(path, separator=' ', kwarg_assignment='='):
        players = []
        with open(path, 'r') as infile:
            for line in infile.xreadlines():
                args = line.strip().split(separator)

                try:
                    population = int(args.pop(0))
                    player_type = args.pop(0)
                except:
                    raise ValueError(('Misformed line %s - need at least '
                                      'player type and population') % line)

                kwargs = {}
                for kwarg in args:
                    key, value = kwarg.split(kwarg_assignment)
                    try:
                        value = ast.literal_eval(value)
                    except ValueError:
                        pass
                    kwargs[key] = value

                for _ in xrange(int(population)):
                    instance = Player.create_from_string(player_type, **kwargs)
                    players.append(instance)

        return Game(players)


def score(a, b):
    if a == HUNT and b == HUNT:
        return 0
    if a == HUNT and b == SLACK:
        return -3
    if a == SLACK and b == HUNT:
        return 1
    if a == SLACK and b == SLACK:
        return -2


def run_game(infile, save_to):
    game = Game.create_from_file(infile)

    outfile = ('%s-%s'
               % (os.path.basename(os.path.splitext(infile)[0]),
                  util.now()))
    outpath = os.path.join(save_to, outfile)

    with open(outpath + '.log', 'w') as logfile:
        game.run(logfile=logfile)

    print '\n'.join(['%d: %s' % (rank, repr(player))
                     for (rank, player) in game.player_ranking()])

    plot.plot_game(game, save_to=outpath + '.png')


if __name__ == '__main__':
    argv = sys.argv[1:]

    if len(argv) == 0:
        sys.exit('Please specify at least one input file')

    opts = [arg for arg in argv if arg.startswith('-')]
    args = [arg for arg in argv if not arg.startswith('-')]

    save_to = util.get_opt('save-to', opts, default=os.getcwd())
    use_threads = util.get_opt('use-threads', opts, default=True)

    util.mkdir(save_to)

    if use_threads:
        threads = []
        for infile in args:
            thread = threading.Thread(target=run_game, args=(infile, save_to))
            thread.daemon = True
            threads.append(thread)
            thread.start()
        util.join_threads(threads)

    else:
        for infile in args:
            run_game(infile, save_to)
