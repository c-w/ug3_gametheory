# coding=utf8
# -*- coding: utf8 -*-
# vim: set fileencoding=utf8 :

import collections
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

import util


def create_colormap(keys):
    colors = [(r / 255.0, g / 255.0, b / 255.0, 1.0)
              for r, g, b in util.generate_random_colors(len(keys))]
    return dict(zip(keys, colors))


def create_figure(winx=800, winy=600, dpi=80):
    try:
        winx, winy = util.get_screen_size()
    except OSError:
        pass
    plt.figure(figsize=(winx / dpi, winy / dpi), dpi=dpi)


def create_legend(ax, plot_info, colormap, label_fun):
    handles = [plt.Rectangle((0, 0), 1, 1, fc=colormap[key])
               for key in plot_info.keys()]
    labels = [label_fun(key, value) for (key, value) in plot_info.items()]
    ax.legend(handles, labels, loc='center', ncol=3)
    ax.get_yaxis().set_visible(False)
    ax.get_xaxis().set_visible(False)
    ax.set_frame_on(False)


def save_figure(save_to):
    plt.tight_layout()
    plt.savefig(save_to, bbox_inches='tight')


def plot_game(game, save_to):
    players_by_rank = game.player_ranking()
    rounds = range(1, game.round_num + 1)
    player_stats = collections.defaultdict(lambda: util.MutableNamedTuple(int))
    min_rank = min([rank for (rank, _) in players_by_rank])
    for rank, player in players_by_rank:
        player_repr = repr(player.wrapped)
        player_stats[player_repr].population += 1
        if rank == min_rank:
            player_stats[player_repr].winners += 1
    player_types = player_stats.keys()

    # setup plot and maximize
    colormap = create_colormap(player_types)
    create_figure()

    # plot food over time
    ax1 = plt.subplot2grid((5, 1), (0, 0), rowspan=2)
    max_food = 0
    for _, player in players_by_rank:
        rounds_lived, food = zip(*zip(rounds, player.food_history))
        ax1.plot(rounds_lived, food, color=colormap[repr(player.wrapped)])
        max_food = max(max_food, max(food))
    ax1.set_xlim(0, max(rounds))
    ax1.set_ylim(0, max_food)
    ax1.set_xlabel('Rounds')
    ax1.set_ylabel('Food')

    # plot reputation over time
    ax2 = plt.subplot2grid((5, 1), (2, 0), rowspan=2)
    for _, player in players_by_rank:
        rounds_lived, reputation = zip(*zip(rounds,
                                            player.reputation_history))
        ax2.plot(rounds_lived, reputation,
                 color=colormap[repr(player.wrapped)])
    ax2.set_xlim(0, max(rounds))
    ax2.set_ylim(-0.05, 1.05)
    ax2.set_xlabel('Rounds')
    ax2.set_ylabel('Reputation')

    # create legend
    ax3 = plt.subplot2grid((5, 1), (4, 0))
    label_fun = (lambda player_type, player_info:
                 '%s%s (%d)' % ('* ' * player_info.winners, player_type,
                                player_info.population))
    create_legend(ax3, player_stats, colormap, label_fun)

    # save graph
    save_figure(save_to)


def plot_stats(stats, save_to):
    # normalise ranks
    all_ranks = [rank for (_, _, _, rank) in stats]
    all_ranks = util.transpose(all_ranks)
    all_ranks = [[x / float(sum(row)) for x in row] for row in all_ranks]
    all_ranks = util.transpose(all_ranks)

    # objectify stats
    player_stats = collections.defaultdict(lambda: util.MutableNamedTuple())
    for (i, (name, mean, sdev, _)) in enumerate(stats):
        player_stats[name].mean = mean
        player_stats[name].sdev = sdev
        player_stats[name].ranks = all_ranks[i]

    # setup plot and maximize
    colormap = create_colormap(player_stats.keys())
    create_figure()

    # plot ranks
    width = 1.0
    ax1 = plt.subplot2grid((5, 1), (0, 0), rowspan=4)
    xs = xrange(1, len(all_ranks[0]) + 1)
    bottom = [0] * len(xs)
    for name, player_info in player_stats.items():
        ys = player_info.ranks
        ax1.bar(xs, ys, bottom=bottom, color=colormap[name], width=width)
        bottom = util.vector_sum(ys, bottom)
    plt.xticks([x + width / 2.0 for x in xs], map(str, xs))
    plt.xlim(min(xs), max(xs))
    plt.ylim(0, 1)
    plt.xlabel('Rank')
    plt.ylabel('Probability')

    # create legend
    ax2 = plt.subplot2grid((5, 1), (4, 0))
    label_fun = (lambda player_type, player_info:
                 u'%s (μ=%.2f, σ=%.2f)'
                 % (player_type, player_info.mean, player_info.sdev))
    create_legend(ax2, player_stats, colormap, label_fun)

    # save graph
    save_figure(save_to)
