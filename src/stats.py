import collections
import os
import random
import sys

import util
import plot
from player import Player
from game import Game


def random_population(player_types, max_instances, min_instances=1):
    players = []
    for player_type in player_types:
        if hasattr(player_type, '__simulator_num_instances__'):
            num_instances = player_type.__simulator_num_instances__
        else:
            if hasattr(player_type, '__simulator_max_instances__'):
                max_instances = player_type.__simulator_max_instances__
            if hasattr(player_type, '__simulator_min_instances__'):
                min_instances = player_type.__simulator_min_instances__

            if min_instances >= max_instances:
                num_instances = min_instances
            else:
                num_instances = random.randint(min_instances, max_instances)

        for _ in xrange(num_instances):
            players.append(player_type())

    return players


def run_games(num_games, max_instances, save_to):
    outpath_id = ('%s-runs-%d-diversity-%d'
                  % (util.now(), num_games, max_instances))
    outpath_base = os.path.join(save_to, outpath_id)
    util.mkdir(outpath_base)

    # run games and collect performance data
    player_types = Player.all_subclasses()
    performances = collections.defaultdict(list)
    for i in xrange(num_games):
        sys.stderr.write('\rsimulating game %d/%d' % ((i + 1), num_games))
        outpath = os.path.join(outpath_base, 'game-%d' % i)

        players = random_population(player_types, max_instances)
        game = Game(players)

        game.run()
        plot.plot_game(game, save_to=outpath + '.png')

        ranking = game.player_ranking()
        for rank, player in ranking:
            performances[type(player.wrapped)].append(rank)

    # output performance statistics as csv
    max_rank = max(util.flatten(performances.values()))
    all_ranks = xrange(1, max_rank + 1)

    stats = []
    for player_type, player_ranks in performances.items():
        player_name = repr(player_type())

        # basic statistics over all ranks
        rank_mean = util.mean(player_ranks)
        rank_sdev = util.sdev(player_ranks)

        # figure out distribution of positions the player attained
        num_ranks = len(player_ranks)
        times_placed_per_rank = util.counter(player_ranks)
        rank_distribution = []
        for rank in all_ranks:
            try:
                times_placed = times_placed_per_rank[rank]
            except KeyError:
                times_placed = 0

            rank_distribution.append(times_placed / float(num_ranks))

        stats.append((player_name, rank_mean, rank_sdev, rank_distribution))
    stats.sort(key=lambda t: t[1])  # sort by best average rank first

    csv_header = ('# player-type, mean-rank, sdev-rank, %s'
                  % ', '.join(util.to_string(all_ranks)))
    csv_lines = ['%s,%f,%f,%s'
                 % (name, rank_mean, rank_sdev,
                    ','.join(util.to_string(rank_distribution)))
                 for (name, rank_mean, rank_sdev, rank_distribution)
                 in stats]

    outpath_results = os.path.join(outpath_base, 'summary')
    with open(outpath_results + '.csv', 'w') as csvfile:
        csvfile.write(csv_header + '\n')
        csvfile.write('\n'.join(csv_lines))

    plot.plot_stats(stats, save_to=outpath_results + '.png')


if __name__ == '__main__':
    argv = sys.argv[1:]

    if len(argv) < 2:
        sys.exit(('Please specify number of games to run and maximum number of'
                  'instances of each player type to create'))

    opts = [arg for arg in argv if arg.startswith('-')]
    args = [arg for arg in argv if not arg.startswith('-')]

    num_games = int(args[0])
    num_instances = int(args[1])
    save_to = util.get_opt('save-to', opts, default=os.getcwd())

    util.mkdir(save_to)

    run_games(num_games, num_instances, save_to)
