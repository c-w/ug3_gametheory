import ast
import collections
import datetime
import math
import os
import random


class MutableNamedTuple(collections.defaultdict):
    def __init__(self, *args, **kwargs):
        super(MutableNamedTuple, self).__init__(*args, **kwargs)
        self._initialized = True

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        if hasattr(self, ' _initialized'):
            super(MutableNamedTuple, self).__setitem__(name, value)
        else:
            super(MutableNamedTuple, self).__setattr__(name, value)


def rchop(s, suffix):
    """Removes |suffix| from the end of string |s| if it is present

    >>> rchop('foobar', 'bar')
    'foo'

    >>> rchop('foo', 'bar')
    'foo'

    >>> rchop('foobarbaz', 'bar')
    'foobarbaz'
    """

    if s.endswith(suffix):
        return s[:-len(suffix)]
    return s


def join_threads(threads):
    """Join threads in interruptible manner"""

    for thread in threads:
        while thread.isAlive():
            thread.join(5)


def now():
    return datetime.datetime.now().strftime('%Y%m%d%H%M%S')


def percentile(li, P):
    """Returns the element at the |P|-th percentile of |li|

    >>> percentile([15, 20, 35, 40, 50], 0.30)
    20
    >>> percentile([15, 20, 35, 40, 50], 0.35)
    20
    >>> percentile([15, 20, 35, 40, 50], 0.40)
    35
    """

    N = len(li)
    n = round(P * N + 0.5)
    return sorted(li)[int(n) - 1]


def mean(li):
    """Computes the arithmetic mean of a sequence of numbers

    >>> mean([4, 36, 45, 50, 75])
    42.0
    """

    return sum(li) / float(len(li))


def sdev(li):
    """Computes the standard deviation of a sequence of numbers

    >>> sdev([2, 4, 4, 4, 5, 5, 7, 9])
    2.0
    """

    mu = mean(li)
    return math.sqrt(mean([(x - mu) ** 2 for x in li]))


def mkdir(path):
    """Creates a directory (and all of its parents) if it does not yet exist"""

    try:
        os.stat(path)
    except OSError:
        os.makedirs(path)
    except:
        pass


def get_opt(opt_name, opts, default=None):
    opt_flag = '--%s=' % opt_name
    matches = [opt.split('=')[1] for opt in opts if opt.startswith(opt_flag)]
    return matches.pop(0) if len(matches) > 0 else default


def get_screen_size():
    """Returns the resolution of the currently active monitor (Unix only)"""

    if not 'DISPLAY' in os.environ:
        raise OSError('No display environment variable present')

    xrandr = os.popen('xrandr | grep "*"').readlines()[0]
    resolution = xrandr.strip().split()[0]
    width, height = resolution.split('x')
    return int(width), int(height)


def parse_dict(arg_string, arg_sep=' ', kv_sep='='):
    """Parses arg_string into a string-keyed dictionary. Converts values to
    literal structures wherever possible.

    >>> parse_dict('a=1 b=True c=foo') == {'a': 1, 'b': True, 'c': 'foo'}
    True
    """

    d = {}
    for arg in arg_string.split(arg_sep):
        k, v = arg.split(kv_sep)
        try:
            v = ast.literal_eval(v)
        except ValueError:
            pass
        d[k] = v
    return d


def counter(iterable):
    """Returns a dictionary d where for each element x in $iterable d[x] is the
    number of times x occurs in $iterable.

    >>> singles = [1, 2, 3]
    >>> c = counter(singles)
    >>> all([c[x] == 1 for x in singles])
    True

    >>> doubles = 'abcdabcd'
    >>> c = counter(doubles)
    >>> all([c[x] == 2 for x in doubles])
    True

    >>> tuple = (1, 2, 2, 3, 3, 3)
    >>> c = counter(tuple)
    >>> c[1] == 1 and c[2] == 2 and c[3] == 3
    True
    """

    counts = collections.defaultdict(int)
    for elem in iterable:
        counts[elem] += 1
    return counts


def to_string(iterable):
    return [str(elem) for elem in iterable]


def flatten(li):
    """Merges a nested list into a flat list.

    >>> flatten([[1], [2], [3]])
    [1, 2, 3]

    >>> flatten([1, [2], [[3]]])
    [1, 2, 3]
    """

    def is_iterable(obj):
        return (isinstance(obj, collections.Iterable) and
                not isinstance(obj, basestring))

    def flatten_generator(li):
        for el in li:
            if is_iterable(el):
                for sub in flatten(el):
                    yield sub
            else:
                yield el

    return list(flatten_generator(li))


def transpose(matrix):
    """Returns the transpose of a matrix

    >>> m1 = [[1, 2, 3], [1, 2, 3], [1, 2, 3]]
    >>> transpose(m1)
    [[1, 1, 1], [2, 2, 2], [3, 3, 3]]

    >>> m2 = [[1, 2], [3, 4], [5, 6]]
    >>> transpose(m2)
    [[1, 3, 5], [2, 4, 6]]

    >>> transpose(transpose(m1)) == m1
    True
    >>> transpose(transpose(m2)) == m2
    True
    """

    matrix_t = zip(*matrix)
    return [list(row) for row in matrix_t]


def vector_sum(v1, v2):
    """Returns the sum of vectors v1 and v2

    >>> v1 = [1, 2, 3]
    >>> v2 = [4, 5, 6]
    >>> vector_sum(v1, v2)
    [5, 7, 9]

    >>> vector_sum(v1, v1) == [x * 2 for x in v1]
    True
    >>> vector_sum(v2, v2) == [x * 2 for x in v2]
    True
    """

    return [a + b for (a, b) in zip(v1, v2)]


def random_int(lo, hi):
    """Returns a random integer in the open interval (lo, hi)

    >>> lo = 10
    >>> hi = 100
    >>> all([lo < random_int(lo, hi) < hi for _  in xrange(100000)])
    True
    """

    return random.randint(lo + 1, hi - 1)


def hsv_to_rgb(h, s, v):
    """Maps a hue-saturation-value tripled into RGB colorspace.
    h = [0..360], s = [0..1], v = [0..1]

    >>> hsv_to_rgb(0, 1, 1)
    (255, 0, 0)
    >>> hsv_to_rgb(120, 1, 1)
    (0, 255, 0)
    >>> hsv_to_rgb(240, 1, 1)
    (0, 0, 255)
    """

    c = v * s
    x = c * (1.0 - abs(((h / 60.0) % 2) - 1.0))

    r1, g1, b1 = 0, 0, 0
    if 0 <= h < 60:
        r1, g1, b1 = c, x, 0
    if 60 <= h < 120:
        r1, g1, b1 = x, c, 0
    if 120 <= h < 180:
        r1, g1, b1 = 0, c, x
    if 180 <= h < 240:
        r1, g1, b1 = 0, x, c
    if 240 <= h < 300:
        r1, g1, b1 = x, 0, c
    if 300 <= h < 360:
        r1, g1, b1 = c, 0, x

    m = v - c
    r = r1 + m
    g = g1 + m
    b = b1 + m
    return (int(255 * r), int(255 * g), int(255 * b))


def generate_random_colors(num, s=0.5, v=0.95):
    """Generates some number of random colors in RGB space"""

    phi_conjugate = 2.0 / (1.0 + math.sqrt(5))
    h = random.random()
    for _ in xrange(num):
        h += phi_conjugate
        h %= 1
        yield hsv_to_rgb(360 * h, s, v)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
